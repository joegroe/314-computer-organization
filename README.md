# 314 Computer Organization

* Project 1 - Bit Manipulation in C
* Project 2 - Bit Shifting and Logic
* Project 3 - Int Array and Pointers
* Project 4 - Assembly Introduction
* Project 5 - Swap in Y86 Assembly
* Project 6 - Bubble Sort in Assembly
* Project 7 - CPU Time Calculation
* Project 8 - Direct-Mapped Cache Simulator
